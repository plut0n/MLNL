import csv
import os
import torch
import numpy as np
import nonloc_tb as nltb
import pandas as pd
#import umap as um
#from bokeh.plotting import figure, output_file, show
#from bokeh.models import CategoricalColorMapper, ColumnDataSource
#from bokeh.palettes import Category10
from time import gmtime, strftime
from torch.utils.data.dataset import Dataset

#--- DataGen and inheritance ---#

class Data_Gen(object):
    def __init__(self,N=None,m=None,d=None,R=None,nb=None,save=None):
        """ Generate local and nonlocal point into symmetrized space.

        Parameters
        ----------
        N : int
            Number of partite
        m : int
            Number of input per partite
        d : int
            Number of output per partite
        R : int
            Higher order of body correlator to compute
        nb : list
            Two int, each one represent number of point local/non-local to generate
        save : None/True/int
            - if ``type(save)`` is ``None``, generated data would just be stored in attribute data of the class
            - if ``type(save)`` is ``True``, generated data would be stored in a csv file with the formate date-(Non)Local_numberofpoint.csv
            - if ``type(save)`` is ``int``, generated data would be stored in a csv file w/ the same format as above but every ``save`` point, and then, merge into one file (all temporary file will be deleted once merged)"
        """
        if N is None:
            self.N=3
            print("Using default value for N: 3")
        else:
            self.N=N
        if m is None:
            self.m=2
            print("Using default value for m: 2")
        else:
            self.m=m
        if d is None:
            self.d=2
            print("Using default value for d: 2")
        else:
            self.d=d
        if R is None:
            self.R=2
            print("Using default value for R: 2")
        else:
            self.R=R
        if nb is None:
            self.nb=[100,100]
            print("Using default value for nb: [100,100]")
        else:
            if type(nb) is not list:
                raise TypeError("nb has to be a list")
            elif len(nb) is not 2:
                raise UserWarning("nb needs to have exactly two elements")
            elif nb[0]<0 or nb[1]<0:
                raise UserWarning("nb can't have negative element")
            else:
                self.nb=nb

    def merge_csv(self,epoch,save,date):
        """Merge multiple csv.

        Parameters
        ----------
        epoch : int
            number of files
        save : int
            number of line per file
        date : str
            Path and beginning of file name
        """
        #Open new file to merge data into
        fout_L=open(date+'-Merge-Local_'+str(self.N)+'_'+str(self.nb[0])+'.csv','a')
        fout_nL=open(date+'-Merge-NonLocal_'+str(self.N)+'_'+str(self.nb[1])+'.csv','a')
        #Writing down everything
        for line in open(date+'-Local_'+str(save)+'.csv'):
            fout_L.write(line)
        for line in open(date+'-NonLocal_'+str(save)+'.csv'):
            fout_nL.write(line)
        #Removing file after usage
        os.system('rm '+date+'-Local_'+str(save)+'.csv')
        os.system('rm '+date+'-NonLocal_'+str(save)+'.csv')
        for _ in range(1,epoch):
            f = open(date+'-Local_'+str(save*(_+1))+'.csv')
            g = open(date+'-NonLocal_'+str(save*(_+1))+'.csv')
            for line in f:
                fout_L.write(line)
            for line in g:
                fout_nL.write(line)
            f.close()
            g.close()
            os.system('rm '+date+'-Local_'+str(save*(_+1))+'.csv')
            os.system('rm '+date+'-NonLocal_'+str(save*(_+1))+'.csv')
        fout_L.close()
        fout_nL.close()

class Data_Origin(Data_Gen):
    def __init__(self,N=None,m=None,d=None,R=None,nb=None,save=None):
        Data_Gen.__init__(self,N,m,d,R,nb)
        #Save is None -> Just store generated data into class attribute
        if save is None:
            self.data=[[nltb.L(self.N,self.m,self.d,self.R,self.nb[0])],[nltb.nonL(self.N,self.m,self.d,self.R,self.nb[1])]]
        #Save is True -> Store generated data into a csv file
        elif save is True:
            self.data=[[nltb.L(self.N,self.m,self.d,self.R,self.nb[0])],[nltb.nonL(self.N,self.m,self.d,self.R,self.nb[1])]]
            date='../data/'+strftime("%d_%m_%Y_%H_%M0", gmtime())+'-Origin_'
            np.savetxt(date+'-Local_'+str(self.nb[0])+'.csv',self.data[0][0][1],delimiter=',')
            np.savetxt(date+'-NonLocal_'+str(self.nb[1])+'.csv',self.data[1][0][1],delimiter=',')
        #Save is an int -> Every Save, we store generated data into a csv file (hit and run init again), before merging everything into one file
        elif type(save) is int:
            date='../data/'+strftime("%d_%m_%Y_%H_%M0", gmtime())+'-Origin_'
            if self.nb[0]==self.nb[1]:
                epoch=int(self.nb[0]/save)
                self.data=[]
                for _ in range(epoch):
                    self.data.append([[nltb.L(self.N,self.m,self.d,self.R,save)],[nltb.nonL(self.N,self.m,self.d,self.R,save)]])
                    np.savetxt(date+'-Local_'+str(save*(_+1))+'.csv',self.data[_][0][0][1],delimiter=',')
                    np.savetxt(date+'-NonLocal_'+str(save*(_+1))+'.csv',self.data[_][1][0][1],delimiter=',')
            self.merge_csv(epoch,save,date)

class Data_Sym(Data_Gen):
    def __init__(self,N=None,m=None,d=None,R=None,nb=None,save=None,S=None):
        Data_Gen.__init__(self,N,m,d,R,nb)
        self.P=nltb.PS(self.N,self.m,self.R)
        droplev=True if self.N % 2 is 0 else False
        #Save is None -> Just store generated data into class attribute
        if save is None:
            self.data=[[nltb.hit_n_run(self.P,self.nb[0],droplev=droplev)],[nltb.nonL(self.N,self.m,self.d,self.R,self.nb[1],S=S)]]
        #Save is True -> Store generated data into a csv file
        elif save is True:
            self.data=[[nltb.hit_n_run(self.P,self.nb[0],droplev=droplev)],[nltb.nonL(self.N,self.m,self.d,self.R,self.nb[1],S=S)]]
            date='../data/'+strftime("%d_%m_%Y_%H_%M0", gmtime())+'-Sym-N_'+str(self.N)
            np.savetxt(date+'-Local_'+str(self.nb[0])+'.csv',self.data[0][0],delimiter=',')
            np.savetxt(date+'-NonLocal_'+str(self.nb[1])+'.csv',self.data[1][0][1],delimiter=',')
        #Save is an int -> Every Save, we store generated data into a csv file (hit and run init again), before merging everything into one file
        elif type(save) is int:
            date='../data/'+strftime("%d_%m_%Y_%H_%M0", gmtime())+'-Sym-N_'+str(self.N)
            if self.nb[0]==self.nb[1]:
                epoch=int(self.nb[0]/save)
                self.data=[]
                for _ in range(epoch):
                    self.data.append([[nltb.hit_n_run(self.P,save,droplev=droplev)],[nltb.nonL(self.N,self.m,self.d,self.R,save,S=S)]])
                    np.savetxt(date+'-Local_'+str(save*(_+1))+'.csv',self.data[_][0][0],delimiter=',')
                    np.savetxt(date+'-NonLocal_'+str(save*(_+1))+'.csv',self.data[_][1][0][1],delimiter=',')
            self.merge_csv(epoch,save,date)

#--- DataFile ---#

class Data_File(Dataset):
    def __init__(self, L_file, NL_file,domain=None,droplev=None,loc_only=None):
        """ Dataset (Pytorch format) for local and non local generated from a file. Fill attribute data from csv files.

        Parameters
        ----------
        L_file : string
            Name of file containing local points
        NL_file : string
            Name of file containing nonlocal points
        """
        if type(L_file) is not str:
            raise TypeError("L_file should be a string")
        else:
            self.L_file=L_file
        if type(NL_file) is not str:
            raise TypeError("NL_file should be a string")
        else:
            self.NL_file=NL_file
        self.data_L=[]
        self.data_NL=[]
        self.labels=[] #0 for local, 1 for non local
        with open(self.L_file) as local:
            readCSV = csv.reader(local, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
            for row in readCSV:
                self.data_L.append(row)
                self.labels.append(0)
        if loc_only is None:
            with open(self.NL_file) as nlocal:
                readCSV = csv.reader(nlocal, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
                for row in readCSV:
                    self.data_NL.append(row)
                    self.labels.append(1)
        self.labels=torch.tensor(self.labels)
        if droplev:
            data=np.array(self.data_L+self.data_NL)
            data=np.delete(data,(0,1),1)
            self.data=torch.tensor(data.tolist())
        else:
            self.data=torch.tensor(self.data_L+self.data_NL)
        if domain is not None:
            self.domain=self.labels
        self.classes=('local','nonlocal')
        self.num_samples=len(self.data)

    def __getitem__(self, index, verbose=None):
        """Return ``index`` element of the dataset

        Parameters
        ----------
        index : int
            index of element to return
        verbose : none/anything
            verbose if not None

        Returns
        -------
        data[index] : torch.tensor
            ``index`` element of the dataset. A pytorch tensor
        lables[index] : torch.tensor
            And its corresponding label
        """
        if verbose is not None:
            print ('\tcalling Dataset:__getitem__ @ idx=%d'%index)
        return self.data[index],self.labels[index]

    def __len__(self):
        """Return the length (or number of sample) of the dataset
        """
        return self.num_samples


class Data_N_File(Dataset):
    def __init__(self, L_folder=None, NL_folder=None, N_start=None, N_end=None,even=None, loc_only=None,dicke=None):
        """ Dataset (Pytorch format) for local and non local generated from a list of file. Fill attribute data from csv files. Add 'domain' label (number of partite).

        Parameters
        ----------
        L_folder : string or None
            Name of folder containing files containing local correlations
        NL_folder : string or None
            Name of folder containing files containing nonlocal correlations
        N_start : int
            Minimum number of partite to start with
        N_end : int
            Maximum number of partite to end up with
        even: bool or None
            When ``even`` is None, we take N from ``N_start`` to ``N_end``. If ``even`` is ``True`` we remove odd N (inv. with ``False``)
        dicke: bool or None
            If ``true`` we consider having to have data for body-correlator of the order of at least 2 (no lev, set to 0 in init). Default ``None``.

        Notes
        -----
        Architecture of data folder as to be : folder_local/Ni.csv with i being the number of partite (eq. for non local)
        """
        if type(L_folder) is not str and L_folder is not None:
            raise TypeError("L_file should be a string")
        elif L_folder is None:
            self.L_folder='../data/train/local/rejection/'
        else:
            self.L_folder=L_folder
        if type(NL_folder) is not str and NL_folder is not None:
            raise TypeError("NL_file should be a string")
        elif NL_folder is None:
            self.NL_folder='../data/train/nonlocal/tura/'
        else:
            self.NL_folder=NL_folder
        if N_start > N_end :
            raise UserWarning("N_start shouldn't be bigger than N_end")
        self.data_L=[]
        self.data_NL=[]
        self.domains_L=[]
        self.domains_NL=[]
        for i,n in enumerate(range(N_start,N_end+1)):
            if even is not None:
                if even and n % 2 !=0:
                    continue
                elif not even and n % 2 == 0:
                    continue
            lev=[0,0] if (n % 2 is 0 and dicke is not None) else []
            with open(self.L_folder+'N'+str(n)+'.csv') as local:
                readCSV = csv.reader(local, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
                for row in readCSV:
                    self.data_L.append(lev+row)
                    self.domains_L.append(i)
            if loc_only is None:
                with open(self.NL_folder+'N'+str(n)+'.csv') as nlocal:
                    readCSV = csv.reader(nlocal, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
                    for row in readCSV:
                        self.data_NL.append(lev+row)
                        self.domains_NL.append(i)
        self.data=torch.tensor(self.data_L+self.data_NL)
        self.labels=torch.tensor([0 for _ in self.data_L]+[1 for _ in self.data_NL]) #0 for local, 1 for for nonlocal
        self.domains=torch.tensor(self.domains_L+self.domains_NL)
        self.classes_label=('local','nonlocal')
        self.classes_domain=tuple([str(n) for n in range(N_start, N_end+1)])
        self.classes=('local','nonlocal')
        self.num_samples=len(self.data)

    def __getitem__(self, index, verbose=None):
        """Return ``index`` element of the dataset

        Parameters
        ----------
        index : int
            index of element to return
        verbose : none/anything
            verbose if not None

        Returns
        -------
        data[index] : torch.tensor
            ``index`` element of the dataset. A pytorch tensor
        lables[index] : torch.tensor
            And its corresponding label
        """
        if verbose is not None:
            print ('\tcalling Dataset:__getitem__ @ idx=%d'%index)
        return self.data[index],self.labels[index],self.domains[index]

    def __len__(self):
        """Return the length (or number of sample) of the dataset
        """
        return self.num_samples


#def umap_vis(d):
#    """Visualize data with UMAP"""
#    file_name=("../data/foo")
#    output_file(file_name)
#    data_set=um.UMAP().fit_transform(d.data)
#    source = ColumnDataSource(dict(
#        x_data = [data[0] for data in data_set],
#        y_data = [data[1] for data in data_set],
#        label_data = [int(e) for e in d.labels]
#    ))
#    cmap = CategoricalColorMapper(factors=['Local','Non Local'], palette=Category10[10])
#    umap_plot_ = figure(title="Fancy plot")
#    umap_plot_.circle(x='x_data',
#        y='y_data',
#        source=source,
#        color={"field": 'label_data', "transform": cmap},
#            legend='label_data')
#    show(umap_plot_)
