#--- Dependencies ---#
import random
import copy
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from torch.autograd import Variable, Function
from torch.utils.data.dataloader import DataLoader

#--- Project Dependencies ---#
import nonloc_tb as loc
import data as dt

#--- Tools ---#
class ReverseLayerF(Function):
    @staticmethod
    def forward(ctx, x, alpha):
        ctx.alpha = alpha
        return x.view_as(x)

    @staticmethod
    def backward(ctx, grad_output):
        output = grad_output.neg() * ctx.alpha
        return output, None

#--- Neural Network ---#
class MLP(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        """MLP with one hidden layer, and using ReLU has activation function"""
        super(MLP, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """ Feed forward the MLP.

        Parameters
        ----------
        x : tensor
            Data to feed forward

        Returns
        -------
        out : tensor
            Activation value at the last layer
        """
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        return out

class DANN(nn.Module):
    """DANN (feature selection + domain classifier + label classifier"""
    def __init__(self,input_size,domain_size,struct_neurone=None):
        super(DANN, self).__init__()
        self.struct_neurone=struct_neurone if struct_neurone is not None else [[20,10],[20,10],[20]]
        #feature extractor
        self.feature = nn.Sequential()
        self.feature.add_module('f_fc1', nn.Linear(input_size, 20))
        self.feature.add_module('f_relu1', nn.ReLU(True))
        self.feature.add_module('f_fc2', nn.Linear(20, 10))
        self.feature.add_module('f_relu2', nn.ReLU(True))

        #Label Classifier -> Local/Non-Local
        self.class_classifier = nn.Sequential()
        self.class_classifier.add_module('l_fc1', nn.Linear(10, 20))
        self.class_classifier.add_module('l_relu1', nn.ReLU(True))
        self.class_classifier.add_module('l_fc2', nn.Linear(20, 10))
        self.class_classifier.add_module('l_relu2', nn.ReLU(True))
        self.class_classifier.add_module('l_fc3', nn.Linear(10, 2))

        #Domain classifier -> Guess N (between 3 and 3+domain_size)
        self.domain_classifier = nn.Sequential()
        self.domain_classifier.add_module('d_fc1', nn.Linear(10, 20))
        self.domain_classifier.add_module('d_relu1', nn.ReLU(True))
        self.domain_classifier.add_module('d_fc2', nn.Linear(20, domain_size))

    def forward(self, input_data, alpha, save_efeature=None):
        feature = self.feature(input_data)
        reverse_feature = ReverseLayerF.apply(feature, alpha)
        class_output = self.class_classifier(feature)
        domain_output = self.domain_classifier(reverse_feature)
        return class_output, domain_output

class DANN_evol(nn.Module):
    """DANN for neuro evolution"""
    def __init__(self,feature, label, domain):
        super(DANN_evol, self).__init__()
        #Feature extractor
        self.feature=nn.Sequential()
        for _ in range(len(feature)-1):
            self.feature.add_module('f_fc'+str(_), nn.Linear(feature[_],feature[_+1]))
            self.feature.add_module('f_relu'+str(_), nn.ReLU(True))

        #Label claissifier
        if feature[-1]!=label[0]:
            raise UserWarning("Layer between two connected MLP need to have the same size")
        self.class_classifier=nn.Sequential()
        for _ in range(len(label)-2):
            self.class_classifier.add_module('l_fc'+str(_), nn.Linear(label[_],label[_+1]))
            self.class_classifier.add_module('l_relu'+str(_), nn.ReLU(True))
        self.class_classifier.add_module('l_fc'+str(len(label)-2), nn.Linear(label[-2],label[-1]))

        #Domain classifier
        if feature[-1]!=domain[0]:
            raise UserWarning("Layer between two connected MLP need to have the same size")
        self.domain_classifier=nn.Sequential()
        for _ in range(len(domain)-2):
            self.domain_classifier.add_module('l_fc'+str(_), nn.Linear(domain[_],domain[_+1]))
            self.domain_classifier.add_module('l_relu'+str(_), nn.ReLU(True))
        self.domain_classifier.add_module('l_fc'+str(len(domain)-2), nn.Linear(domain[-2],domain[-1]))

    def forward(self, input_data, alpha):
        feature = self.feature(input_data)
        reverse_feature = ReverseLayerF.apply(feature, alpha)
        class_output = self.class_classifier(feature)
        domain_output = self.domain_classifier(reverse_feature)
        return class_output, domain_output, reverse_feature

#--- Main class ---#
class NL_MLP(object):
    def __init__(self,N_start,N_end=None,dir_loc='../data/train/local/rejection_norm/',dir_nonloc='../data/train/nonlocal/tura_norm/',dicke=None,even=None,batch_size=None):
        """
        Parameters
        ----------
            N_start : int
                Number of partite to start with (training set)
            N_end : None or int
                Number of partite to end up with (training set). Default is set to ``None`` wich mean the MLP will only be trained on ``N_start``.
            dir_loc : str
                csv file path to local data
            dir_nonloc : str
                csv file path to nonlocal data
            dicke : None or True
                cf. Data_N_File class init.
            even : None or True or false
                cf. Data_n_File class init. Has to be set to ``True`` or ``False`` if even/odd data don't have the same feature space (i.e. not having LEV/having LEV)
            batch_size : int
                Batch size
        """
        #Check cuda availability
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        #Loading dataset:
        if N_end is None:
            N_end=N_start
        self.dset=dt.Data_N_File(L_folder=dir_loc, NL_folder=dir_nonloc, N_start=N_start, N_end=N_end, even=even, dicke=dicke) #Dataset (L/NL file containe Body correlator, label (+/- for local/nonlocal), and domain (N)
        self.dim=len(self.dset.__getitem__(0)[0]) #Dim of one sample -> will be used as the number of input nodes
        #Set batch_size if None
        if batch_size is None:
            self.batch_size=self.dset.__len__()//100
        else:
            self.batch_size=batch_size
        #Load Training set
        self.train_set=DataLoader(self.dset,self.batch_size,shuffle=True,num_workers=4,
                pin_memory=True #Cuda
                )
        #We need the user to init the mlp, this a watchdog to check if it was done
        self.mlp_init=False

    def init_mlp(self,hidden_size,learning_rate,num_classes=None,weight=None):
        """Initialize the neural network.

        Parameters
        ----------
        hidden_size : int
            number of nodes in hidden layer
        num_classes : int
            Number of classes (default ``2`` :math:`\Rightarrow` local and nonlocal)
        learning_rate : int
            Learning rate

        Notes
        -----
        Neural network (MLP) is initaliaze to use ``cuda`` (GPU). Gradient descent is done via Adam. We also use the cross entropy loss (cost function).
        """
        if num_classes is None:
            num_classes=2
        self.mlp_=MLP(self.dim, hidden_size, 2).to(self.device)
        self.criterion_ = nn.CrossEntropyLoss().to(self.device) if weight is None else nn.CrossEntropyLoss(weight=weight).to(self.device)
        self.optimizer_ = torch.optim.Adam(self.mlp_.parameters(), lr=learning_rate)
        self.mlp_init=True

    def train(self,num_epochs,verbose=None):
        """Train Neural Network (MLP).

        Parameters
        ----------
        num_epochs : int
            number of time to iterate on the dataset
        verbose : None or True
            When set to ``True``, epoch number, step and loss are display. Default is ``None``.
        """
        if not self.mlp_init:
            raise UserWarning("No NN has been initialized.")
        self.err_label_=[]
        for epoch in range(num_epochs):
            loss=[]
            for i, (data, labels, domains) in enumerate(self.train_set):
                # Convert torch tensor to Variable
                data = data.to(self.device)
                labels = labels.to(self.device)
                # Forward + Backward + Optimize
                self.optimizer_.zero_grad()  # zero the gradient buffer
                outputs = self.mlp_(data)
                loss.append(self.criterion_(outputs, labels))
                loss[i].backward()
                self.optimizer_.step()
                if (i+1) % 10 == 0 and verbose:
                    print ('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
                           %(epoch+1, num_epochs, i+1, self.train_set.__len__(), loss[i].item()))
            self.err_label_.append((sum(loss)/len(loss)).item())
        self.err_label_=np.array(self.err_label_)

    def test(self,N_start,N_end=None,dir_loc='../data/test/local/rejection_norm/',dir_nonloc='../data/test/nonlocal/tura_norm/',verbose=None,ret=None,dicke=None,loc_only=None):
        """Test our trained Neural Network.

        Parameters
        ----------
        N_start : int
            Minimum number of partite to start with
        N_end : None or int
            Maximum  number of partite to end up with (if ``None``, test just predict for N_start)
        dir_loc : str
            file path to local point to test
        dir_nonloc : str
            file path to non local point to test
        verbose : None or True
            When set to ``True``, display local and nonlocal accuracy.
        ret : None or True
            When set to ``True``, return, in a tuple, number of correctly classified local and nonlocal point, and total accuracy of the network.
        """
        if N_end is None:
            N_end=N_start
        self.dtest=dt.Data_N_File(L_folder=dir_loc, NL_folder=dir_nonloc, N_start=N_start, N_end=N_end,loc_only=loc_only,dicke=dicke)
        self.test_set_=DataLoader(self.dtest,self.batch_size,shuffle=False)
        correct = 0
        total = 0
        loc_correct = 0
        nonloc_correct = 0
        with torch.no_grad(): #Memory efficieny
            for data,labels,domains in self.test_set_:
                data = data.to(self.device)
                labels = labels.to(self.device)
                outputs = self.mlp_(data)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
                loc_correct+=sum([1 if predicted[i]==lab and lab==0 else 0 for i,lab in enumerate(labels)])
                nonloc_correct+=sum([1 if predicted[i]==lab and lab==1 else 0 for i,lab in enumerate(labels)])
        print('Accuracy of the network on the '+str(self.dtest.__len__())+' test point: %8.4f %%' % ( 100 * correct / total))
        if verbose:
            print('Local correctly labeled:    [%d/%d]' % (loc_correct,(self.test_set_.dataset.labels == 0 ).sum().item() ))
            print('NonLocal correctly labeled: [%d/%d]' % (nonloc_correct,(self.test_set_.dataset.labels == 1 ).sum().item() ))
        if ret:
            return loc_correct, nonloc_correct, (100 * correct / total)


    def unit_test(self,p,label,ret=None):
        """Predict the label of a given point ``p`` and compare it to ``label``.

        Parameters
        ----------
        p : list
            point to test
        label : int
            ``0`` local, ``1`` nonlocal
        """
        with torch.no_grad():
            data=torch.tensor([p]).to(self.device)
            label=torch.tensor(label).to(self.device)
            outputs = self.mlp_(data)
            _, predicted = torch.max(outputs.data, 1)
            correct = (predicted == label).sum().item()
        verbose='Correct guess :)' if correct == 1 else 'Wrong guess :('
        if ret:
            return correct
        print(verbose)

    def plot_err(self):
        """ Plot err of label and domain classifer"""
        fig=plt.subplot()
        fig.plot(self.err_label_,label='label err')
        fig.set(xlabel='num_epoch', ylabel='err', title='mean err evolution per epoch')
        fig.legend()
        fig.show()

class NL_DANN(object):
    def __init__(self,N_start,N_end,dir_loc='../data/train/local/rejection/',dir_nonloc='../data/train/nonlocal/tura/',batch_size=None,even=None,dicke=None):
        self.N_start=N_start
        self.N_end=N_end
        #Check cuda availability
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        #Loading dataset:
        self.dset=dt.Data_N_File(L_folder=dir_loc, NL_folder=dir_nonloc, N_start=N_start, N_end=N_end, even=even, dicke=dicke) #Dataset (L/NL file containe Body correlator, label (+/- for local/nonlocal), and domain (N)
        self.dim=len(self.dset.__getitem__(0)[0]) #Dim of one sample -> will be used as the number of input nodes
        #Set batch_size if None
        if batch_size is None:
            self.batch_size=self.dset.__len__()//100
        else:
            self.batch_size=batch_size
        #Load Training set
        self.train_set=DataLoader(self.dset,self.batch_size,shuffle=True,num_workers=4,
                pin_memory=True #Cuda
                )
        #We need the user to init the dann, this is a failsafe to check if it was done
        self.dann_init=False

    def init_dann(self,learning_rate=None, load=None, weight=None,feature=None,label=None,domain=None):
        """Initialize the (dann) neural network.

        Parameters
        ----------
        learning_rate : int
            Learning rate
        input_size : int
            number of nodes in first_layer (default is ``5``)
        load : str or None
            filename containing a pretrain model to load
        weight : torch.tensor
            Tensor of dimension 2 (number of class), used to rescale weight of the loss function

        Notes
        -----
        Neural network (DANN) is initaliaze to use ``cuda`` (GPU). Gradient descent is done via Adam. We also use the cross entropy loss (cost function).
        """
        self.learning_rate=0.001 if learning_rate is None else learning_rate
        self.feature=[5,20,10] if feature is None else feature
        self.label=[10,20,10,2] if label is None else label
        self.domain=[10,20,(self.N_end-self.N_start)+1] if domain is None else domain
        self.dann_=DANN_evol(self.feature,self.label,self.domain).to(self.device)
        if load is not None:
            self.dann_.load_state_dict(torch.load(load))
        self.loss_class_ = nn.CrossEntropyLoss().to(self.device) if weight is None else nn.CrossEntropyLoss(weight=weight).to(self.device)
        self.loss_domain_ = nn.CrossEntropyLoss().to(self.device)
        self.optimizer_ = torch.optim.Adam(self.dann_.parameters(), lr=self.learning_rate)
        #Compute the gradient for sub-network (each sequential)
        for p in self.dann_.parameters():
            p.requires_grad = True
        self.inv_space=[]
        self.dann_init=True

    def train(self,num_epochs,verbose=None):
        """Train the DANN.

        Parameters
        ----------
        num_epochs : int
            number of time to iterate on the dataset
        """
        self.err_label_avg_=[]
        self.err_domain_avg_=[]
        if not self.dann_init:
            raise UserWarning("No NN has been initialized.")
        for epoch in range(num_epochs):
            err_label=[]
            err_domain=[]
            for i,(data,label,domain) in enumerate(self.train_set):
                p = float(i + epoch * self.train_set.__len__()) / num_epochs / self.train_set.__len__()
                alpha = 2. / (1. + np.exp(-10 * p)) - 1 #Used for inversed gardiant descent
                data=Variable(data).to(self.device)
                label=Variable(label).to(self.device)
                domain=Variable(domain).to(self.device)
                label_output, domain_output, _ = self.dann_(input_data=data, alpha=alpha)
                err_label.append(self.loss_class_(label_output, label))
                err_domain.append(self.loss_domain_(domain_output, domain))
                err=err_label[i]+err_domain[i]
                self.optimizer_.zero_grad()
                err.backward()
                self.optimizer_.step()
                if (i+1) % 10 == 0 and verbose is not None:
                    print('epoch: [%d/%d], Step [%d/%d], err_label: %f, err_domain: %f' \
                        % (epoch+1, num_epochs, i+1, self.train_set.__len__(), err_label[i].cpu().data.numpy(),
                        err_domain[i].cpu().data.numpy()))
            self.err_label_avg_.append((sum(err_label)/len(err_label)).item())
            self.err_domain_avg_.append((sum(err_domain)/len(err_domain)).item())
        self.err_label_avg_=np.array(self.err_label_avg_)
        self.err_domain_avg_=np.array(self.err_domain_avg_)

    def test(self,N_start,N_end=None,dir_loc='../data/test/local/rejection/',dir_nonloc='../data/test/nonlocal/tura/',dicke=None,verbose=None,ret=None,loc_only=None,get_inv_space=None):
        """Test our trained Neural Network.

        Parameters
        ----------
        N_start : int
            Minimum number of partite to start with
        N_end : None or int
            Maximum  number of partite to end up with (if ``None``, test just predict for N_start)
        dir_loc : str
            file path to local point to test
        dir_nonloc : str
            file path to non local point to test
        verbose : None or True
            When set to ``True``, display local and nonlocal accuracy.
        ret : None or True
            When set to ``True``, return, in a tuple, number of correctly classified local and nonlocal point, and total accuracy of the network.
        """
        if N_end is None:
            N_end=N_start
        self.dtest=dt.Data_N_File(L_folder=dir_loc, NL_folder=dir_nonloc, N_start=N_start, N_end=N_end,loc_only=loc_only,dicke=dicke)
        self.test_set_=DataLoader(self.dtest,self.batch_size,shuffle=True)
        correct = 0
        total = 0
        loc_correct = 0
        nonloc_correct = 0
        inv_space_n=[]
        with torch.no_grad(): #Memory efficieny
            for i,(data,label,domain) in enumerate(self.test_set_):
                data = data.to(self.device)
                label = label.to(self.device)
                class_output, _, reverse_feature = self.dann_(input_data=data, alpha=0)
                _, pred = torch.max(class_output.data, 1)
                inv_space_n.append([reverse_feature,pred,label])
                total += label.size(0)
                correct += (pred == label).sum().item()
                loc_correct+=sum([1 if pred[i]==lab and lab==0 else 0 for i,lab in enumerate(label)])
                nonloc_correct+=sum([1 if pred[i]==lab and lab==1 else 0 for i,lab in enumerate(label)])
        if verbose is not False:
            print('Accuracy of the network on the '+str(self.dtest.__len__())+' test point: %8.4f %%' % ( 100 * correct / total))
            print('Local correctly labeled:    [%d/%d]' % (loc_correct,(self.test_set_.dataset.labels == 0 ).sum().item() ))
            print('NonLocal correctly labeled: [%d/%d]' % (nonloc_correct,(self.test_set_.dataset.labels == 1 ).sum().item() ))
        if ret is not None:
            return loc_correct, nonloc_correct, (100 * correct / total)
        if get_inv_space is not None:
            self.inv_space.append(inv_space_n)
            return inv_space_n

    def unit_test(self,p,label):
        """Predict the label of a given point ``p`` and compare it to ``label``.

        Parameters
        ----------
        p : list
            point to test
        label : int
            ``0`` local, ``1`` nonlocal
        """
        label=torch.tensor(label).to(self.device)
        if len(p) == 3 :
            print('P is of size 3, assuming p[0] & p[1] = 0')
            p=[0,0]+p
        data=torch.tensor([p]).to(self.device)
        correct=0
        with torch.no_grad():
            class_output, _ = self.dann_(input_data=data,alpha=0)
            pred= class_output.data.max(1, keepdim=True)[1]
            correct+=pred.eq(label.data.view_as(pred)).cpu().sum().item()
        verbose='Correct guess :)' if correct == 1 else 'Wrong guess :('
        print(verbose)
        return correct

    def plot_err(self,figfile=None,show=None):
        """ Plot err of label and domain classifer"""
        plt.figure()
        plt.plot(self.err_label_avg_,label='label err')
        plt.plot(self.err_domain_avg_, label='domain err')
        plt.xlabel('num_epoch')
        plt.ylabel('err')
        plt.title('mean err evolution per epoch')
        plt.legend()
        if figfile is None:
            figfile="../report/plot/plot_err_dann.png"
        if show:
            plt.show()
        plt.savefig(figfile)

    def save(self, savefile=None):
        """Save the trained DANN

        Parameters
        ----------
        savefile : str
            relative path and name of the file were the nn state will be save
        """
        if self.dann_init is False:
            raise UserWarning("No init network to save")
        if savefile is None:
            savefile='../data/models/dann_'+str(self.N_start)+'_'+str(self.N_end)+'.pkl'
        torch.save(self.dann_,savefile)

    def load(self, savefile):
        """Load a save model (Trained)

         Parameters
        ----------
        savefile : str
            relative path and name of the file were the nn state will be save

        Notes
        -----
        A pretrained model can be load on both a CPU or GPU (cuda) architecture, it's device-agnostic.
        """
        self.dann_=torch.load(savefile, map_location=self.device)

    def genetic_opt(self,num_gen=None, mut_impact=None, pop_size=None, train=None, verbose=None):
        """Find optimal configuration of loss classes weights via a pseudo-genetic algorithm (optimisation via metaheuristic search)

        Parameters
        ----------
        num_gen : int or None
            Number of generation to evolv through (default 100)
        mut_impact : float or None
            Compare to a nomral genetic algorithm, mutation will happen to child only, and to all of them. Mutation are a normal distributed center in 1, number multiply to previous weight. num_rate is the width of the gaussian
            Rate ([0;1]) of mutation event (default 0.01)
        pop_size : int or None
            Size of the population (number of dann per epoch) (default 20)
        train : int or None
            Number of epochs to train each DANN on.
        verbose : True or None
            When ``True``, print population weights to follow the evolution

        Notes
        -----
        I implemented a meatheuristic search to find the optimal rescaling weight of the cost function since what we want is a perfect classification of local point while having a good overall accuracy.
        """
        #Init genetic parameter
        num_gen = 100 if num_gen is None else num_gen
        mut_impact = 0.1 if mut_impact is None else mut_impact
        pop_size= 20 if pop_size is None else pop_size
        train= 10 if train is None else train
        cross_rate= 0.8
        max_w=5;min_w=0
        #Starting with a random population
        pop=[torch.tensor(list(np.random.random(2)*max_w)) for _ in range(pop_size)]
        for gen in range(num_gen):
            print(gen+1," generation...")
            perf=[]
            for i,w in enumerate(pop):
                self.init_dann(weight=w)
                self.train(train+gen)
                perf.append(np.array([self.test(n,dicke=True,ret=True) for n in range(3,9)]))
                if (i+1) % (pop_size//10) == 0 and verbose:
                    print(i+1,' individual trained')
            score=np.array([[np.mean(_[:,0]),np.mean(_[:,2])] for _ in perf])
            norm_loc=max(score[:,0]);norm_acc=max(score[:,1])
            score[:,0]/=norm_loc;score[:,1]/=norm_acc
            fscore=np.argsort([sum(_) for _ in score])[::-1]
            pop=[pop[i] for i in fscore[:(pop_size//2)]]
            cross=random.sample(range((pop_size//2)), (pop_size//2))
            mut_pop=[torch.tensor([_[0].item(),pop[j][1].item()]) for _,j in zip(pop,cross)]
            if verbose:
                print("pop:\n",pop,"\n mut_pop:\n",mut_pop)
            mut_pop=[torch.tensor([_[0].item()*np.random.normal(loc=1,scale=mut_impact),_[1].item()*np.random.normal(loc=1,scale=mut_impact)]) for _ in mut_pop]
            pop+=mut_pop
        self.loss_class_ = nn.CrossEntropyLoss(weight=pop[0]).to(self.device)
        print("Dann init with loss_class_ weight:", pop[0])
        self.pop=pop #If anyone want to access to last population weights

    def inv_space_plot(self):
        if len(self.inv_space)==0:
            raise UserWarning("inv_space not assigned, please test DANN with get_inv_space set to something")
        xyz=np.array([_[0].tolist() for foo in self.inv_space for _ in foo])
        sh=xyz.shape
        xyz=xyz.reshape(sh[1]*sh[0],sh[2])
        pred=(np.array([_[1].tolist() for foo in self.inv_space for _ in foo])).reshape(sh[0]*sh[1])
        label=(np.array([_[1].tolist() for foo in self.inv_space for _ in foo])).reshape(sh[0]*sh[1])
        cdict={0: 'blue', 1: 'orange'}
        ldict={0: 'local', 1: 'nonlocal'}
        fig=plt.figure()
        if sh[2]==3:
            print("Plotting 3D...")
            ax = fig.add_subplot(111, projection='3d')
            for l in np.unique(label):
                ix = np.where(label==l)
                ax.scatter(xyz[ix][:,0], xyz[ix][:,1], xyz[ix][:,2], c = cdict[l], label = ldict[l], s = 100)
                ax.legend()
            plt.show()
        elif sh[2]==2:
            print("Plotting 2D...")
            ax = fig.add_subplot(111)
            for l in np.unique(label):
                ix = np.where(label==l)
                ax.scatter(xyz[ix][:,0], xyz[ix][:,1] , c = cdict[l], label = ldict[l], s = 100)
                ax.legend()
            plt.show()
        else:
            print("Seaborn pair plot not implemented yet")



def Neuroevolution(N,num_gen=None,mut_impact=None, pop_size=None, train=None):
    """Neuroevolution of DANNs : using a genetic algorithm try to find the structure of DANN that peforms best (best accuracy in overall classification + best accuracy on local point)

    Parameters
    ----------
    N : int
        number of number of partites DANN is trained on
    num_gen : int or None
        number of generation to iterate through. Default ``50``
    mut_impact : int or None
        frequency of mutation happenning at each generation. Default ``0.15``
    pop_size : int or None
        number of DANNs composing the population. Default ``20``
    train : int or None
        Epoch to train each DANN on. Default ``50``

    Note
    ----
    Cross rate is fixed at ``.5`` for now.
    """
    num_gen = 50 if num_gen is None else num_gen
    mut_impact = 0.15 if mut_impact is None else mut_impact
    pop_size= 20 if pop_size is None else pop_size
    train= 50 if train is None else train
    max_neurone=20
    max_hidden_layer=2
    pop_feature=[[5]+[np.random.randint(3,max_neurone) for _ in range(np.random.randint(2,max_hidden_layer+2))] for _ in range(pop_size)]
    pop_label=[[_[-1]]+[np.random.randint(3,max_neurone) for foo in range(np.random.randint(1,max_hidden_layer+1))]+[2] for _ in pop_feature]
    pop_domain=[[_[-1]]+[np.random.randint(3,max_neurone) for bar in range(np.random.randint(1,max_hidden_layer+1))]+[N] for _ in pop_feature]
    pop=[[feature]+[label]+[domain] for feature,label,domain in zip(pop_feature,pop_label,pop_domain)]
    danns=[NL_DANN(3,6) for _ in range(pop_size)]
    for gen in range(num_gen):
        print("Generation [%d/%d]" % (gen+1,num_gen))
        for dann,_ in zip(danns,pop):
            dann.init_dann(feature=_[0],label=_[1],domain=_[2])
        for dann in danns:
            dann.train(train+gen)
        #Eval the perf of DANNs
        perf=np.array([[dann.test(n,ret=True) for n in range(3,11)] for dann in danns])
        score=np.array([[np.mean(_[:,0]),np.mean(_[:,2])] for _ in perf])
        norm_loc=max(score[:,0]);norm_acc=max(score[:,1])
        score[:,0]/=norm_loc;score[:,1]/=norm_acc
        fscore=np.argsort([sum(_) for _ in score])[::-1]
        if gen==num_gen-1:
            print(pop)
            print("\nBest configuration: ",pop[fscore[0]],"\n",danns[fscore[0]].dann_)
        #Let's save the best half of them
        pop=[pop[i] for i in fscore[:(pop_size//2)]]
        #And they reproduce, because biology
        cross=[random.sample(range((pop_size//2)), (pop_size//2)) for _ in range(3)]
        children=[[pop[i][0][:]]+[pop[j][1][:]]+[pop[k][2][:]] for i,j,k in zip(cross[0],cross[1],cross[2])]
        for _ in children:
            _[1][0]=_[0][-1]
            _[2][0]=_[0][-1]
        pop+=children
        #mutation frequency
        mut=np.array([[1 if np.random.random(1)<=mut_impact else 0 for foo in range(3)] for _ in range(pop_size)])
        for index,_ in enumerate(mut):
            if (_==1).any():
                for foo,mutation in enumerate(_):
                    if(mutation==1):
                        if foo==0:
                            pop[index][foo]=[pop[index][foo][0]]+[min(20,max(3,j+np.random.randint(4)-2)) for i,j in enumerate(pop[index][foo]) if i!=0]
                            pop[index][1][0]=copy.deepcopy(pop[index][0][-1])
                            pop[index][2][0]=copy.deepcopy(pop[index][0][-1])
                        else:
                            last_layer=len(pop[index][foo])
                            pop[index][foo]=[pop[index][foo][0]]+[min(20,max(3,j+np.random.randint(4)-2)) for i,j in enumerate(pop[index][foo]) if i!=0 and i!=(last_layer-1)]+[pop[index][foo][-1]]
