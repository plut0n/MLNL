"""
Non-Locality ToolBox
====================
This module contains useful functions for the studies of (non)locality.
Here's a non exhaustive list of what one can found in this module:
    - Tools for computational convex geometry : membership problem using LP, sampling uniformely in a convex set (hit'n'run)
    - Computation of local polytope, symmetrized space polytope (using n-body correlator)
    - Computation of n-body correlators
    - Generation of random measurement
    - and so on
"""

import mosek
import numpy as np
import qutip as Q
import pandas as pd
import picos as pcs
import seaborn as sns
import matplotlib.pyplot as plt
from itertools import product, permutations, combinations, combinations_with_replacement
from scipy.special import binom

#--- Main functions ---#

#--- Tools ---#

def plotdist(p,lim):
    """Plot distribution of ``p``'s features

    Parameters
    ----------
    p : array_like
        2D array each row is a sample, each column a feature
    lim : int
        Plot from ``p[0,feature]`` to ``p[lim,feature]``

    Notes
    -----
    This function is to be use via an iPython kernel or similar, with matplotlib magic command initialized, i.e.

        >>> %matplotlib qt
        >>> plotdist(p,lim)
    """
    sns.set(color_codes=True)
    for _ in range(len(p[0])):
        x=p[:lim,_]
        sns.distplot(x)

def plotproj(p,q=None,even=None):
    """Plot 2D projection of data in higher dimension.

    Parameters
    ----------
    p : array_like
        data each row is an observation, each column is a variable
    q : array_like or None
        same as above / None

    Notes
    -----
    To use only for 2 measurements, 2 output, and LEV+2-body correlator (5 dimensions)
    This function is to be use via an iPython kernel or similar, with matplotlib magic command initialized, i.e.

        >>> %matplotlib qt
        >>> plotproj(p,q)
    """
    sns.set(color_codes=True)
    R=1 if even is None else 2
    comb=np.array(['S'+str(_) for r in range(R,3) for _ in combinations_with_replacement([0,1],r)])
    p=pd.DataFrame(p,columns=comb)
    if q is not None:
        q=pd.DataFrame(q,columns=comb)
        lab_l=['local']*p.shape[0]
        lab_nl=['nonlocal']*q.shape[0]
        p['l/nl']=lab_l
        q['l/nl']=lab_nl
        _=pd.concat([p,q])
        sns.pairplot(_,markers='+',hue='l/nl',palette='plasma',diag_kind='kde')
    else:
        sns.pairplot(p,markers='+')

#--- Toolbox on Computational Convex Geometry ---#

def is_local(Polytope, point):
    r"""Membership Problem.

    Test using linear programming if a given ``point`` p is in a given convex set (polytope) ``Polytope``. This is done by solving:

    .. math::

       \min_{x} && \quad 1 \\
               s.t. \quad Polytope^{T}.x && = p \quad \forall x \quad \textrm{unitary} \\
                                 x && \geqslant 0

    Parameters
    ----------
    PS : array_like
        Convex polytope vertices (array of array).
    point : array_like
        Point to test (array). Dimensions has to be the same as one vertices of PS.

    Returns
    -------
    bool
        ``True`` if ``point`` is in ``PS``, ``False`` otherwise.

    Notes
    -----
    Use an SDP (Linear Programming) to solve this. Mosek is the solver used here, see https://mosek.com/ for license.

    """
    islocal=pcs.Problem()
    if type(point) is list:
        point=np.asarray(point)
    elif type(point) is not np.ndarray:
        raise TypeError("Point should be a list or a numpy array")
    s=Polytope[0].size
    sT=Polytope[:,0].size
    if s!=point.size:
        raise UserWarning("Point to test doesn't have the same size of PS's vertices")
    #Defining param for the SDP (identity, point to test and Polytope)
    Ivect=pcs.new_param('Ivect',np.asarray([1 for _ in range(sT)]))
    P=pcs.new_param('P',point)
    Pltp=pcs.new_param('PS',Polytope)
    #Adding variable to minimize over
    x=islocal.add_variable('x',sT)
    #LP conditions
    islocal.add_constraint(Pltp.T*x==P)
    islocal.add_constraint(x.T*Ivect==1)
    islocal.add_constraint(x>0)
    #Minimization over:
    islocal.set_objective('min', 1 | x)
    #Solving -> output either 'optimal' if point is inside the convex polytope or 'prim_infeasable' if outside
    islocal.solve(solver='mosek',verbose=0)
    if islocal.status=='optimal':
        return True
    else:
        return False
    islocal.msk_task.__del__()
    islocal.msk_env.__del__()

#--- Polytope L---#

def vertices(N,m,d):
    """Local Polytope generation.

    Generate :math:`d^{m.N}` vertices of the Local Polytpe for a system with ``N`` partite, each having ``m`` measurement/input and ``d`` output.

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input of one partite
    d : int
        number of output of one partite

    Returns
    -------
    vertices : array_like
        Polytope. Each row is a vertex (corresponding to a deterministic strategy), each column is the :math:`n^{th}` element to that vertice

    """
    D=np.zeros((d**m,m), dtype=int)
    for _ in range(d**m):
        D[_][:]=np.array(np.unravel_index(_,(d,)*m))
    vertices=np.zeros(((d**(m*N),)+(m,)*N+(d,)*N))
    c=0
    for _ in product(range(d**m), repeat=N):
        for x in product(range(m), repeat=N):
            vertices[(c,)+x+tuple([D[_[i]][x[i]] for i in range(N)])]=1
        c+=1
    shape=np.prod(np.delete(vertices.shape[:],0,0))
    return vertices.reshape(vertices.shape[0],shape)

def raw_vertices(N,m,d):
    """ Same as vertices but in a human readable format (used for debugging)

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input of one partite
    d : int
        number of output of one partite

    Returns
    -------
    vertices: array_like
        Polytope. First index is the strategy. Second index the input for the first partite, ... , :math:`N^{th}` index input of the :math:`N^{th}` partite. :math:`(N+1)^{th}` index output for first partite ... :math:`(N+N)^{th}` index output for the :math:`N^{th}` partite

    Notes
    -----
    Human reable because, i.e. in a 2 partite scenario with two in/out, if you want the value of :math:`p(01\mid 10)` for a strategy math:`c`, you just need to call the vertices followed by ``[c,x,y,a,b]``

        >>> P=raw_vertices(2,2,2)
        >>> a=0;b=1;x=0;y=1
        >>> c=5
        >>> P[c,x,y,a,b]
        1
    """
    D=np.zeros((d**m,m), dtype=int)
    for _ in range(d**m):
        D[_][:]=np.array(np.unravel_index(_,(d,)*m))
    vertices=np.zeros(((d**(m*N),)+(m,)*N+(d,)*N))
    for c,_ in enumerate(product(range(d**m), repeat=N)):
        for x in product(range(m), repeat=N):
            vertices[(c,)+x+tuple([D[_[i]][x[i]] for i in range(N)])]=1
    return vertices

def CG_vertices(N,m,d):
    r"""Local Polytope using Collin-Gisin notation (space).

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input of one partite
    d : int
        number of output of one partite

    Returns
    -------
    array_like
        all vertices caraterising the polytope, each array is a detereministic strategy
    label : list
        see below

    Notes
    -----
    Polytope. Each row is a vertex corresponding to a deterministic strategy.
    Columns are :math:`p(a_1 \ldots a_n \mid x_0 \ldots x_n) \quad n \subset \{1,\ldots,N\}` (or :math:`p(0\ldots 0\mid 0\ldots 0),p(0\ldots 0\mid 0\ldots 1),\ldots ,p((d-1)\ldots (d-1)\mid m\ldots m))` followed by correlators from :math:`(N-1)` bodies to local expectation values). I.e. for a tripartite scenario w/ 2 measurement & 2 output:

    .. math::

        && p_{abc}(000\mid 000),p_{abc}(000\mid 001),\ldots ,p_{abc}(000\ldots 111), \\
            && p_{ab}(00\mid 00),p_{ab}(00\mid 01),p_{ab}(00\mid 10),p_{ab}(00\mid 11),p_{ac}(00\mid 00),\ldots ,p_{bc}(00\mid 00), \\
            && p_a(0\mid 0),p_a(0\mid 1),p_b(0\mid 0),\ldots ,p_c(0\mid 1)

    Label correspond's to the used partite, input, and ouput of each columns. One can construct vertices with explicit name using pandas.

        >>> p,label = nonloc_tb.cg_vertices(2,2,2)
        >>> pandas.DataFrame(data=p[0][0],index=label)
                               0
        (0, 1): (0, 0)|(0, 0)  1
        (0, 1): (0, 0)|(0, 1)  1
        (0, 1): (0, 0)|(1, 0)  1
        (0, 1): (0, 0)|(1, 1)  1
        (0,): (0,)|(0,)        1
        (0,): (0,)|(1,)        1
        (1,): (0,)|(0,)        1
        (1,): (0,)|(1,)        1

    """
    #Useful parameter -> number of information needed per partite & list of 0 to N-1:
    size=(d-1)*m
    ln=[_ for _ in range(N)]
    #Compute all possible combination of N partite's single body (local) marginal distribution for every input and every output -1
    onebody=[list(_) for _ in product(range(2), repeat=N*size)]
    ref=[[str(tuple([c]))+': '+str(tuple([a]))+'|'+str(tuple([x])) for c in range(N) for a in range(d-1) for x in range(m)]]
    onebody.reverse()
    mar=[np.array(onebody)]
    #r-body marginal with r from 2 (if N>2) to N-1
    for r in range(1,N):
        comb=list(combinations(ln,r+1))
        index=list(product(range(size), repeat=r+1))
        mar.append(np.array([[np.product([ _[c[i]*size+ind[i]] for i in range(r+1) ]) for c in comb for ind in index] for _ in mar[0]]))
        ref.append([str(c)+': '+str(a)+'|'+str(x) for c in comb for a in product(range(d-1), repeat=r+1) for x in product(range(m), repeat=r+1)])
    ref.reverse()
    label=[item for sub in ref for item in sub]
    return np.array([np.concatenate(tuple(mar[N-1-i][j] for i in range(N))) for j in range(mar[0][:,0].size)]),label

def get_p_known(index,a,x,lp):
    """
    Return for given in/output(s) the corresponding r-body collerator :math:`p(a_{r0}\ldots a_{r[-1]}|x_1\ldots x_N)` iif that :math:`p` is already present into the Collin-Gisin space (<=> iif all output are not equal to :math:`(d-1)`)

    Parameters
    ----------
    index : tuple
        array partites index we are interesting into (i.e. ``0`` is Alice, ``1`` is Bob, and so on)
    a : tuple
        output of each of the above partite (index)
    x: tuple
        input of each of the above partite (index)
    lp: pandas_DataFrame
        labeled probalilities vector in Collin-Gisin space

    Returns
    -------
    array_like
        Probabilities to obtain ``a`` given ``x`` (in ``lp``)

    """
    if type(a)==list:
        a=tuple(a)
    return lp.loc[str(index)+': '+str(a)+'|'+str(x)][0]

def sum_a(a,unknown,d):
    """Gives a list of array. When we need to sum on every outputs of some specifics partites given fixed output for other partites.

    I.e. if we want the :math:`\sum_{a}^d p(abc\mid xy)` this function gives: :math:`(0,b,c),(1,b,c),\ldots ,(d-2,b,c)`

    Parameters
    ----------
    a: tuple
        requested output(s)
    unknown: list
        index of a output(s) beeing equal to (``d``-1) (max output)
    d: int
        number of output(s) per partite

    Returns
    -------
    new_a : array_like
        List of every possible outpus ``a`` with combinations of indexes in ``index`` from 0 to ``d``-2 output (all but last)
    """
    a=list(a)
    new_a=[]
    for c,comb in enumerate(product(range(d-1),repeat=len(unknown))):
        new_a.append(a[:])
        for _ in range(len(unknown)):
            new_a[c][unknown[_]]=comb[_]
    return new_a

def get_p(d,index,a,x,lp):
    """
    Return for a given in/output the corresponding :math:`p(a_1\ldots a_N\mid x_1\ldots x_N)`

    Parameters
    ----------
    index : tuple
        partite index i.e ``(0, 3, 4,)``
    a : tuple
        output corresponding to each partite (same order as index)
    x : tuple
        input corresponding to each partite (same order as index)
    lp : pandas_DataFrame
        labeled probalilities vector in Collin-Gisin space

    Returns
    -------
    float64
        Probabilities to obtain ``a`` given ``x`` (in ``lp``)
    """
    #Check if any element is equal to d (not directly a quantities of x, so we need to be compute it)
    if (np.array(a)==(d-1)).any():
        known=[i for i,out in enumerate(a) if not out==(d-1)]
        unknown=[i for i,out in enumerate(a) if out==(d-1)]
        #every-partite output d, then we need to compute 1-all other correlator for every other output
        if len(known)==0:
            return 1-sum([get_p(d,index,a,x,lp) for a in product(range(d), repeat=len(x)) if not (np.array(a)==1).all()])
        #n-partite output d, so we need the N-n body correlator of the other partite
        else:
            tot=get_p_known(tuple([index[k] for k in known]),tuple([a[k] for k in known]),tuple([x[k] for k in known]),lp)
            other=sum([get_p_known(index,a,x,lp) for a in sum_a(a,unknown,d)])
            return tot-other
    else:
        return get_p_known(index,a,x,lp)

def correlator(N,m,d,x,lp):
    """
    Compute correlator for input(s) ``x``

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input of one partite
    d : int
        number of output of one partite
    x : tuple
        partite's input this correlator correspond to

    Returns
    -------
    float64
        Correlator for input(s) ``x``
    """
    return sum([(-1)**(sum(a))*get_p(d,index,a,x,lp) for index in combinations(range(N),len(x)) for a in product(range(d), repeat=len(x))])

#--- Polytope Ps in symmetrized space ---#

def S(PSset,N,m,R,strat):
    """ Compute every :math:`2(N^2+1)` vertices of a given Ps polytope

    Parameters
    ----------
    PSset : array_like
        Polytope generated via vertices
    N : int
        number of partite
    m : int
        number of input of one partite
    d : int
        number of output of one partite
    R : int
        maximum order for body correlator
    strat : array_like
        Every possible deterministic strategy

    Returns
    -------
    LEV : list
        From ``1`` to ``R`` body correlator
    """
    #Number of scalar to compute per correlator order
    #space=[binom(r+m-1,r) for r in range(1,R+2)]
    #space_size=sum(space)
    #S=np.zeros(int(space_size))
    #sign=[np.prod(strat[_,:]) for _ in range(2**m)]
    #Local Expectation Value (S0, S1, etc...)
    LEV=[[sum([strat[i,j]*_[i] for i in range(len(strat[:,0]))]) for j in range(m)] for _ in PSset]
    if R==0:
        return LEV
    #i-body correlators
    for c,_ in enumerate(LEV): #For every strategy (combinations of a,b,c..)
        for i in range(2,R+2): #For every correlator order
            comb=list(combinations_with_replacement(np.array([p for p in range(m)]),i)) #For every combinations of i-input
            for j,combi in enumerate(comb): #For every combinations of partite (without permutation) i.e. for order 2 and 2 partite -> (0,0), (0,1), (1,1)
                S=np.prod([_[k] for k in combi]) #Product of LEV since we are using deterministic strategy
                count=np.bincount(combi)%2
                if (count==0).all():
                    S-=N
                else:
                    sign=[np.prod([foo[index] for index,k in enumerate(count) if k!=0 and index in combi]) for foo in strat]
                    S-=sum([sig*PSset[c][h] for h,sig in enumerate(sign)])
                LEV[c].append(S)
    return LEV

def PS(N,m,R):
    """ Return all vertices defining the Ps polytope (all local models w/o correlators of order higher than ``R``) for ``m`` measurement, and ``N`` partite

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input per partite
    R : int
        higher-order of correlators to take into account

    Returns
    -------
    array_like
        Vertices of the symmetrized polytope Ps

    Notes
    -----
    Atm, this method only works for a system where partites have a binary output
    """
    d=2
    #Find all d**m-tuple of integer that sums up to N and with a least one element being 0
    PSset=[_ for _ in product(range(N+1), repeat=d**m) if sum(_)==N and (np.asarray(_)==0).any()] #Bad computation time, TODO: find a better solution
    #Every possible startegy
    strat=[_ for _ in product(range(d), repeat=m)]
    strat=np.array([[(-1)**_ for _ in strat[i]] for i in range(len(strat))])
    #Compute vertices and return them
    return np.asarray(S(PSset,N,m,R-1,strat))

#--- Sampling in Lc ---#

def intersection(vert,x,d,sT):
    """ Find the interesection between a half line, having an origin in a convex polytope P, and P

    Parameters
    ----------
    vert : array_like
        list of vertices defining of polytope
    x : array
        point inside the polytope
    d : array
        a vector of the unit sphere of the same dimension as the Polytope
    sT : int
        number of vertices caraterising the polytope

    Returns
    -------
    l : float64
        :math:`\lambda` is a sclar such taht :math:`x+\lambda *d` is a point on P surface
    """
    #LP declaration
    inter=pcs.Problem()
    #Parameter: 1 vector, starting point, random direction, list of vertices
    Ivect=pcs.new_param('Ivect',np.asarray([1 for _ in range(sT)]))
    x=pcs.new_param('x',x)
    d=pcs.new_param('d',d)
    vert=pcs.new_param('vert',vert)
    #Variable: l (to max on), and c wich are a list of coef for linear combination betwenen vertices
    l=inter.add_variable('l',1)
    c=inter.add_variable('c',sT)
    #LP conditions: point on a face/edge of our Polytope, l>0, c>0, sum(c)=1
    inter.add_constraint(x+l*d==vert.T*c)
    inter.add_constraint(c.T*Ivect==1)
    inter.add_constraint(l>0)
    inter.add_constraint(c>0)
    inter.set_objective('max', l)
    inter.solve(solver='mosek', verbose=0)
    return inter.get_variable('l').value[0]

def rejection(vert,nb,store_nl=None,nl_nb=None):
    """ Rejection sampling

    Parameters
    ----------

    vert : array_like
        2D array containing polytope's vertices.
    nb : int
        number of point to generate
    store_nl : True or None
        Default is ``None``. When set to ``True``, the first nb element of ``p`` are local corelations, and last elements are nonlocal correlations.

    Returns
    -------
    p : array
        Generated points
    """
    dim=len(vert[0])
    ext=[[max(vert[:,_]),min(vert[:,_])] for _ in range(dim)]
    c=0
    shift=0 if nl_nb==None else 1
    p=[];q=[]
    while(c<nb):
        rnd=np.array([np.random.uniform(low=_[0]-1*shift,high=_[1]+1*shift) for _ in ext])
        if is_local(vert,rnd):
            p.append(rnd)
            c+=1
            if c % (nb//10) == 0:
                print(c,'point generated')
        elif store_nl is True:
            q.append(rnd)
        if len(q)==nl_nb:
            return q
    return p+q

def hit_n_run(vert,nb,ext=None,droplev=None):
    """Hit n Run algo only

    Parameters
    ----------
    vert : array_like
        2D array containing all convex polytope vertices.
    nb : int
        number of point to generate

    Returns
    -------
    p : array
        Generated points
    """
    dim=vert[0].size
    M=vert[:,0].size
    p=[]
    s=[]
    #Find x0 in P^N at random. We use the caratheodory's theorem
    r=np.random.dirichlet(np.ones(dim+1))
    v=np.random.choice(M, dim+1)
    p.append(sum([r[_]*vert[v[_]] for _ in range(dim+1)]))
    #For every remaining point we execute the follow algorithm
    for _ in range(1,nb):
        #Generate a direction d at random from the N dimensional unit sphere
        rand_d=np.random.randn(dim, 1)
        rand_d/=np.linalg.norm(rand_d, axis=0)
        #Find s+/-, intersections between the local polytope and the line passing through x in direction +/-
        S=[p[_-1]+intersection(vert,p[_-1],rand_d,M)*rand_d.T, p[_-1]+intersection(vert,p[_-1],-rand_d,M)*(-rand_d.T)]
        lc=np.random.random(1)[0]
        p.append((lc*S[0]+(1-lc)*S[1])[0])
        if ext is not None:
            s.append(S[0][0])
            s.append(S[1][0])
        if (_+1)%(nb//10)==0:
            print((_+1),' local points sampled')
    tot=np.array(p+s)
    if droplev is True:
        return np.delete(tot,(0,1,),1) #delete column corresponding to LEV
    else:
        return tot


def L(N,m,d,R,nb):
    """Use Hit and Run algorithm in order to sample uniformely from a convex polytope

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input per partite
    d : int
        number of output per partite
    R : int
        higher-order of body correlator to take into account
    nb : int
        numiber of points to generate

    Returns
    -------
    p : array_like
        array of points into the "original" space
    pS : array_like
        array of points into the "symmetrized" space

    Notes
    -----
    S. Vempala. Geometric random walks: A survey. Combinatorial and Computational Geometry, 52:573-612, 2005. [p 2,7]
    """
    #Dim is the space in wich our polytope lies, M is the number of vertices that describes this polytope, x will be our unfiromely random sample
    vert,label=CG_vertices(N,m,d)
    dim=vert[0].size
    M=vert[:,0].size
    p=[]
    #Find x0 in P^N at random. We use the caratheodory's theorem
    r=np.random.dirichlet(np.ones(dim+1))
    v=np.random.choice(M, dim+1)
    p.append(sum([r[_]*vert[v[_]] for _ in range(dim+1)]))
    lp=pd.DataFrame(data=p[0],index=label)
    pS=[[correlator(N,m,d,x,lp) for r in range(1,R+1) for x in combinations_with_replacement(range(m),r)]]
    #For every remaining point we execute the follow algorithm
    for _ in range(1,nb):
        #Generate a direction d at random from the N dimensional unit sphere
        rand_d=np.random.randn(dim, 1)
        rand_d/=np.linalg.norm(rand_d, axis=0)
        #Find s+/-, intersections between the local polytope and the line passing through x in direction +/-
        S=[p[_-1]+intersection(vert,p[_-1],rand_d,M)*rand_d.T, p[_-1]+intersection(vert,p[_-1],-rand_d,M)*(-rand_d.T)]
        lc=np.random.random(1)[0]
        p.append((lc*S[0]+(1-lc)*S[1])[0])
        #Once we have a new point we project it to our symmetrized space
        lp=pd.DataFrame(data=p[_],index=label)
        pS.append([correlator(N,m,d,x,lp) for r in range(1,R+1) for x in combinations_with_replacement(range(m),r)])
    return np.array(p),np.array(pS)

#--- Sampling in Q (nonL) ---#

def rnd_measurement(N,Nx):
    """ Generate random measurement, return i.e. :math:`A_{a}^{x}`, tensor :math:`2N_x` w/ ``a`` rows and ``x`` columns, each element is a 2x2 matrix (measurement), and :math:`\sum_{a}A_{a}^{x}=\mathbb{1}`

    Parameters
    ----------
    N : int
        Dimension of state to measure
    Nx : int
        Number of input (number of pair of measurements to generate)

    Returns
    -------
    m : list
        Array of :math:`2Nx` measurement in ``N`` dimension

    Notes
    -----
    Measurement are generate according to the following algorithm:
        - Take a Haar random unitary matrix in dimension ``N``
        - Take every column of this matrix, this define a random normalized basis in dimension ``N``
        - Compute outer product of some element of the basis :math:`\Rightarrow` Projective measurement
    """
    m=[]
    for i in range(Nx): # for every input
        rand_unit=Q.rand_unitary_haar(N)[:] # generate a random unitary matrix
        basis=[Q.Qobj(rand_unit[:,_]) for _ in range(N)] # take every column as being a vector of an orthonormal basis in a n dimensional
        nb=np.random.randint(1,N) # take a number of projector between 1,3 ____ i.e 3
        rnd=np.random.choice(N,N,replace=False) #order of basis selection for next line ___ i.e. [0,3,2,1]
        m0=sum([basis[rnd[j]]*basis[rnd[j]].dag() for j in range(nb)]) # measurement for a=0 as projector being outer product of above basis ___ i.e. |0X0|+|3X3|+|2X2|
        m1=sum([basis[rnd[j]]*basis[rnd[j]].dag() for j in range(nb,N)]) # measurement for a=1 ___ i.e. |1X1|
        m.append([m0,m1]) #each row of a is for a specific input
    return m

def DickeState(N,k):
    r"""Dicke State, :math:`\lvert D^{k}_N \rangle` for N-qubit and k-excitation

    Parameters
    ----------
    N : int
        Number of qubit
    k : int
        Excitation level

    Returns
    -------
    psi : qobj
        DickeState :math:`\lvert D^{k}_N\rangle`
    """
    fock_n=[index for index,_ in enumerate(list(product(range(2), repeat=N))) if sum(_)==k] #range(2) because we're working with qubit
    dim=2**N
    norm=1/np.sqrt(len(fock_n))
    psi=norm*sum([Q.fock(dim,_) for _ in fock_n])
    psi.dims=[[2]*N,[1]*N]
    return psi

def nonL(N,m,d,R,nb,states=None,S=None):
    """ Sampling non local correlation in CG-space

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input per partite
    d : int
        number of output per partite
    R : int
        higher-order of body correlator to take into account
    nb : int
        number of points to generate
    states : None/string
        if None states used will be DickeState, other option are ``'ghz'``, ``'rand'``

    Returns
    -------
    p : array_like
        array of points into the "original" space
    pS : array_like
        array of points into the "symmetrized" space
    """
    c=0
    ln=[_ for _ in range(N)]
    PnonL=[]
    Sq=[]
    if S is None:
        vert=CG_vertices(N,m,d)[0]
    else:
        vert=PS(N,m,S)
    if states is None:
        psi=DickeState(N,N//2)
        droplev=True if N % 2 is 0 else False
    if states=='ghz':
        psi=Q.ghz_state(N)
        droplev=True
    if states=='rand':
        psi=Q.rand_ket_haar(2**N)
        psi.dims=Q.ghz_state(N).dims
        droplev=False
    rho=psi*psi.dag()
    vert=np.delete(vert,(0,1),1) if (droplev and S is not None) else vert
    mod=10 if nb >=10 else 1
    while (c<nb):
        p=[]
        MM=rnd_measurement(2,m)
        M=[MM for _ in range(N)] # All partite have the same measurement -> Perm inv.
        if S is None:
            for r in reversed(range(N)):
                comb=list(combinations(ln,r+1))
                for n in comb:
                    p+=[Q_correlation(N,rho,n,a,x,M) for a in product(range(d-1), repeat=r+1) for x in product(range(m), repeat=r+1)] #We compute correlations
        else:
            p=Q_proj_S(S,N,rho,d,m,M,droplev)
        if not is_local(vert,p): #We check if our p is local (inside CG polytope) -> if not we had them to our list of nonlocal point
            PnonL.append(p)
            Sq.append(Q_proj_S(R,N,rho,d,m,M,droplev)) #We also project that point into the symmetrized space
            c+=1
            if c % (nb//mod) ==0:
                print(c,' nonlocal points generated')
    return np.asarray(PnonL), np.array(Sq)

def tura_nl(N,nb):
    """Use the setup of (J. Tura et al.) to generate non local, in 2-body correlator symmetrized space, correlations for N partites

    Parameters
    ----------
    N : int
        Number of partites
    nb : int
        number of point to generate

    Return
    ------
    nL : array_like
        Array of non local point tino the 2 body correlators space
    """
    nL=[]
    c=0
    k=N/2 if N % 2 is 0 else N//2 +1
    psi=DickeState(N,k)
    rho=psi*psi.dag()
    droplev=False
    vert=PS(N,2,2)
    mod=10 if nb >=10 else 1
    while(c<nb):
        theta=np.random.random(1)[0]*np.pi
        phi=np.random.random(1)[0]*np.pi
        basis0=[Q.Qobj((np.cos(phi)*Q.sigmaz()+np.sin(phi)*Q.sigmax())[:,_]) for _ in range(2)]
        basis1=[Q.Qobj((np.cos(theta)*Q.sigmaz()+np.sin(theta)*Q.sigmax())[:,_]) for _ in range(2)]
        m0=[basis0[i]*basis0[i].dag() for i in range(2)]
        m1=[basis1[i]*basis1[i].dag() for i in range(2)]
        M=[m0,m1]
        M=[M for i in range(N)]
        p=Q_proj_S(2,N,rho,2,2,M,droplev)
        if not is_local(vert,p):
            nL.append(p)
            c+=1
            if c % (nb//mod) ==0:
                print(c,' nonlocal points generated')
    return np.array(nL)

def QC(N,m,d,R,nb,states=None,S=None):
    """ Sampling from quantum states in CG-space (no distinction between local/nonlocal).

    Parameters
    ----------
    N : int
        number of partite
    m : int
        number of input per partite
    d : int
        number of output per partite
    R : int
        higher-order of body correlator to take into account
    nb : int
        number of points to generate
    states : None/string
        if None states used will be DickeState, other option are ``'ghz'``, ``'rand'``

    Returns
    -------
    p : array_like
        array of points into the "original" space
    pS : array_like
        array of points into the "symmetrized" space
    """
    c=0
    ln=[_ for _ in range(N)]
    PnonL=[]
    Sq=[]
    if states is None:
        psi=DickeState(N,N//2)
        droplev=True if N % 2 is 0 else False
    if states=='ghz':
        psi=Q.ghz_state(N)
        droplev=True
    if states=='rand':
        psi=Q.rand_ket_haar(2**N)
        psi.dims=Q.ghz_state(N).dims
        droplev=False
    #droplev: Dropping 0 (local expectation value) for even number of partite if the state is ghz or dicke states (because all local expectation value are 0, might create a base decision function for the learning).
    rho=psi*psi.dag()
    mod=10 if nb >=10 else 1
    while (c<nb):
        p=[]
        MM=rnd_measurement(2,m)
        M=[MM for _ in range(N)] # All partite have the same measurement -> Perm inv.
        for r in reversed(range(N)):
            comb=list(combinations(ln,r+1))
            if S is None:
                for n in comb:
                    p+=[Q_correlation(N,rho,n,a,x,M) for a in product(range(d-1), repeat=r+1) for x in product(range(m), repeat=r+1)] #We compute correlations
            else:
                p=Q_proj_S(S,N,rho,d,m,M,droplev)
        PnonL.append(p)
        Sq.append(Q_proj_S(R,N,rho,d,m,M,droplev)) #We also project that point into the symmetrized space
        c+=1
        if c % (nb//mod) is 0:
            print(c,' nonlocal point generated')
    return np.asarray(PnonL), np.array(Sq)

def Q_S(N,m,rho,M,R):
    """ Compute full projection to a symmetrized space (correlator), only for 2 outputs case.

    Parameters
    ----------
    R : int
        higher order of correlator to compute
    N : int
        Number of Partite
    rho : Qobj_matrix
        Outer product of shared states
    d : int
        number of output per partite
    m : int
        number of input per partite
    M : list
        list of measurement each partite have

    Returns
    -------
    array_like
        Correlator from 0 to ``R``-bodies

    """
    xlist=[_ for _ in range(m)]
    nlist=[_ for _ in range(N)]
    return np.array([sum([sum([(-1)**sum(a)*Q_correlation(N,rho,n,a,x,M) for a in product(range(m), repeat=r)]) for n in permutations(nlist,r)]) for r in range(1,R+1) for x in combinations_with_replacement(xlist,r)])

def Q_correlation(N,rho,n,a,x,M):
    """ Compute marginal correlation of n-body in an ``N`` partite system, given input ``x`` and output ``a``

    Parameters
    ----------
    N : int
        Number of Partite
    rho : Qobj (matrix)
        Outer product of shared states
    n : list
        index of partites we are interesting into
    x : list
        input received by partites
    a : list
        output given by partites
    M : list of Qobj matrix
        list of measurement each partite have

    Returns
    -------
    float64
        Correlation of ``n`` selected bodies, given inputs ``x``, outputs ``a``
    """
    _=Q.tensor(*tuple(M[n[l]][x[l]][a[l]] for l in range(len(n))))
    sub_rho=rho.ptrace(list(n))
    sub_rho.dims=_.dims
    _*=sub_rho #Tr[Tr_{every  partite except n}(\rho).M^{a,x}]
    return _.tr().real

#--- Symmtertized space for Q ---#

def Q_correlator(r,N,rho,d,x,M):
    """ Compute ``r``-body correlator for input ``x`` in a ``N`` partite system, each having measurement :math:`\mathcal{M}^{i}`

    Parameters
    ----------
    r : int
        order of body correlator
    N : int
        Number of Partite
    rho : Qobj (matrix)
        Outer product of shared states
    d : int
        number of output per partite
    x : list
        correlator for inputs ``x`` (warning: permutation are equivalent, i.e. x=[0,1] and x=[1,0])
    M : list
        list of measurement each partite have

    Returns
    -------
    float64
        Correlator of ``r`` selected bodies, given inputs ``x``
    """
    comb=list(permutations([_ for _ in range(N)],r))
    return sum([sum([(-1)**(sum(a))*Q_correlation(N,rho,k,a,x,M) for a in product(range(d), repeat=r)]) for k in comb])

def Q_proj_S(R,N,rho,d,m,M,droplev):
    """ Compute ``1`` to ``R``-body correlator for every possible combinations of input ``x`` in a ``N`` partite system, each having measurement :math:`\mathcal{M}^{i}`

    Parameters
    ----------
    R : int
        higher order of correlator to compute
    N : int
        Number of Partite
    rho : Qobj_matrix
        Outer product of shared states
    d : int
        number of output per partite
    m : int
        number of input per partite
    M : list
        list of measurement each partite have

    Returns
    -------
    array_like
        Correlators from 0 to ``R``-bodies

    """
    ln=[p for p in range(m)]
    return np.array([Q_correlator(r,N,rho,d,x,M) for r in range(1+int(droplev),R+1) for x in list(combinations_with_replacement(ln,r))])

#--- Time/complexity analysis---#

def time_gen_nonL():
    fig = plt.figure()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    ax=plt.axes(projection='3d')
    x=np.array([2.,3.])
    y=np.array([2.,3.,4.,5.])
    k,l=np.meshgrid(x,y)
    z=np.array([(list(np.fromfile('./data/'+str(n)+'N_2345m.out', sep=',')/10)) for n in range(2,4)])
    ax.contour3D(k,l,z.T,500,cmap='cool')
    ax.set_title("Complexity of non-$\mathcal{L}$ correlation generation as of \# of partite and input")
    ax.set_xlabel('\# partite')
    ax.set_ylabel('\# input')
    ax.set_zlabel('time (s)')
    fig.show()

def time_gen_L():
    fig = plt.figure()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    ax=plt.axes(projection='3d')
    x=np.array([2.,3.])
    y=np.array([2.,3.,4.,5.])
    k,l=np.meshgrid(x,y)
    z=np.array([(list(np.fromfile('./data/L'+str(n)+'.out', sep=',')/10)) for n in range(2,4)])
    ax.contour3D(k,l,z.T,200,cmap='plasma')
    ax.set_title("Complexity of $\mathcal{L}$ correlation generation as of \# of partite and input")
    ax.set_xlabel('\# partite')
    ax.set_ylabel('\# input')
    ax.set_zlabel('time (s)')
    fig.show()
