# Machine Learning

We need a feed forward (loops aren't useful relevant in our case) NN with backprop based on  to correct label
(supervised learning). A good choice appear to be MLP (multilayer perceptron).
  
Change in weight driven by:  
~~Simple SGD~~ -> [Gradient descent optimization
algorithms](https://arxiv.org/pdf/1609.04747.pdf) (section 4):
- [ADAM](https://arxiv.org/pdf/1412.6980.pdf) seems intresting, and is widely
  use thanks to its efficient perfomance.
- Adadelta seems also intresting

Nielsen wrote a book on NN and DL (deep learning), that's free and available [here](http://neuralnetworksanddeeplearning.com/index.html)

# Physics

- [energy as a detector of nonlocality of many body spin system](https://arxiv.org/abs/1607.06090) - *Jordi et al.* : Method/Algo to find classical bound with a O(n) complexity. 
